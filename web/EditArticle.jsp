<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: hf57
  Date: 1/02/2019
  Time: 12:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
          crossorigin="anonymous">

    <%--CSS external stylesheet--%>
    <link rel="stylesheet" type="text/css" href="Resources/BlogCSS.css">
    <%--Moves default button text's z index--%>
    <style>
        input[type="file"] {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }
    </style>
    <%--Rich Text Editor import file--%>
    <script src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="Resources/jquery-3.3.1.js"></script>
    <script type="text/javascript">
        function switchImage() {
            var image = document.getElementById("imageToSwap");
            var dropd = document.getElementById("bannerlist");
            // var picurl = document.getElementById("picurl");
            image.src = dropd.value;
            // picurl.value = dropd.value;
        };
        //Function to retrieve id of clicked image and append to html of the editor
        //Image
        $(function () {
            $('img').click(function () {
                var clickedImage = this.id;
                var iframeDOM = $("iframe.cke_wysiwyg_frame").contents();
                var editor = iframeDOM.find(".cke_editable");
                $(editor).append($('#' + clickedImage));
            })
        });
        //Video
        $(function () {
            $('video').click(function () {
                var clickedVideo = this.id;
                $('#' + clickedVideo).attr('controls', true);
                var iframeDOM = $("iframe.cke_wysiwyg_frame").contents();
                var editor = iframeDOM.find(".cke_editable");
                $(editor).append($('#' + clickedVideo));
            })
        });
        //Audio
        $(function () {
            $('audio').click(function () {
                var clickedAudio = this.id;
                $('#' + clickedAudio).attr('controls', true);
                var iframeDOM = $("iframe.cke_wysiwyg_frame").contents();
                var editor = iframeDOM.find(".cke_editable");
                $(editor).append($('#' + clickedAudio));
            })
        });
    </script>

    <title>Create/Edit Article</title>
</head>

<body>
<%@include file="Resources/nav.jsp" %>

<br><br><br><br>

<%--"slide-top" animation from external CSS stylesheet--%>
<div id="slide-top" class="container">

    <%--Create/Edit Article--%>

    <%--Title--%>
    <form action="EditArticleServlet" method="post">
        <select id="bannerlist" onchange="switchImage()" name="banner">
            <option style="font-family: 'Julius Sans One', sans-serif;" value="Photos/Banner_1.jpg">Banner_1</option>
            <option style="font-family: 'Julius Sans One', sans-serif;" value="Photos/Banner_2.jpg">Banner_2</option>
            <option style="font-family: 'Julius Sans One', sans-serif;" value="Photos/Banner_3.jpg">Banner_3</option>
            <option style="font-family: 'Julius Sans One', sans-serif;" value="Photos/Banner_4.jpg">Banner_4</option>
        </select>
        <div id="ArticleTitle" align="center">
            <input style="outline: none; width: 400px; height: 50px; text-align: center" type="text"
                   name="title" placeholder="ENTER TITLE" value="${article.title}">
            <br><br><br>
            <img style="align-content: center" id="imageToSwap"
                 src="${article.banner}"
                 height="270">
        </div>
        <input type="hidden" name="articleID" value=${article.articleID}>
        <br>

        <%--Content--%>
        <label>
            <textarea type="html" name="content">${article.content}</textarea>
        </label>

        <script>
            // Replaces the <textarea id="content"> with a CKEditor
            // instance, using custom configuration.
            CKEDITOR.replace('content', {
                height: 300,
                width: 1100
            });
        </script>
        <br><br>

        <%--Publish Date--%>
        <div id="UpdateArticleButton" align="center">
            <p>CHOOSE PUBLISH DATE</p>
            <jsp:useBean id="now" class="java.util.Date"/>
            <fmt:formatDate var="datenow" value="${now}" pattern="yyyy-MM-dd"/>
            <input type="date" name="publish_time" value="${datenow}">
            <br><br><br>
            <button class="btn btn-outline-secondary  btn-lg" type="submit">SAVE</button>
        </div>
        <br>
    </form>

    <%--User Media Uploads--%>
    <div class="card">
        <div class="card-header" style="background-image: none" align="center">
            <h4>USER MEDIA</h4>
            <small>- Click to add to article</small>
        </div>
        <%--Media Content--%>
        <div class="card-body">
            <div class="card-columns">
                <core:forEach items="${mediaList}" var="url" varStatus="loop">
                    <%--Images--%>
                    <core:if test="${fn:endsWith(url, '.jpeg') || fn:endsWith(url, '.jpg')}">
                        <div class="card">
                            <div class="card-body" align="center">
                                <img id="${'img'+=loop.index}" width="150" height="150"
                                     src="<core:out value="${url}"/>">
                            </div>
                        </div>
                    </core:if>
                    <%--Videos--%>
                    <core:if test="${fn:endsWith(url, '.mp4')}">
                        <div class="card">
                            <div class="card-body" align="center">
                                <video id="${'vid'+=loop.index}" width="250" height="250">
                                    <source src="<core:out value="${url}"/>" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </core:if>
                    <%--Audio--%>
                    <core:if test="${fn:endsWith(url, '.mp3')}">
                        <div class="card">
                            <div class="card-body" align="center">
                                <audio id="${'aud'+=loop.index}" controls style="height: 150px;background-image: url('Resources/Audio_play.png')">
                                    <source src="<core:out value="${url}"/>" type="audio/mp3">
                                </audio>
                            </div>
                        </div>
                    </core:if>
                </core:forEach>
            </div>
        </div>
    </div>

    <%--Upload Buttons--%>
    <div>
        <form action="MediaUploadServlet" method="post" enctype="multipart/form-data">
            <input type="hidden" value="${article.articleID}" name="articleID">
            <br><br>
            <input class="btn btn-outline-secondary  btn-sm" type="file" name="media" size="50" id="file"
            <core:out value="${user eq null ? 'disabled': ''}"/>>

            <label for="file" class="form-control btn btn-outline-secondary  btn-sm"
                   style="padding-top: 10px;height: 40px">CHOOSE A FILE</label>

            <input type="submit" class="btn btn-outline-secondary  btn-sm" value="UPLOAD FILE"
                   style="padding-top: 10px; height: 40px;width: 100%" ;  <core:out
                    value="${user eq null ? 'disabled': ''}"/>>
        </form>
    </div>
</div>
</body>
</html>


package ictgradschool.project.servlets.Utils;

import ictgradschool.project.servlets.Articles.Article;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Sort extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        String sortColumn = request.getParameter("SortColumn");
        ArrayList<Article> Searchedarticles = (ArrayList<Article>) session.getAttribute("Searchedarticles");
        if (sortColumn != null) {
            Comparator<Article> comparator = new ArticleComparator(sortColumn);
            Collections.sort(Searchedarticles, comparator);
            request.setAttribute("Searchedarticles", Searchedarticles);
            request.getRequestDispatcher("SearchResults.jsp").forward(request, response);
        }
    }

    public class ArticleComparator implements Comparator<Article> {
        private String sortField;

        public ArticleComparator(String sortField) {
            this.sortField = sortField;
        }

        public int compare(Article a1, Article a2) {

            if (sortField.equals("title")) {
                return a1.getTitle().compareTo(a2.getTitle());
            } else if (sortField.equals("user_id")) {
                return a1.getUser_id().compareTo(a2.getUser_id());
            } else if (sortField.equals("date")) {
                return a1.getPublish_time().compareTo(a2.getPublish_time());
            } else {
                System.err.println("Meaningless comparison of Articles on unknown " + sortField + ". Not sorting.");
                return 0;
            }
        }
    }
}



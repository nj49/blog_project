package ictgradschool.project.servlets.Accounts;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class User implements Serializable {
    private String user_id, lname, fname, country, url, password;
    private String bio;
    private List<String> pics;
    private byte[] salt;
    private int iteration;
    java.sql.Date dob;

    public User() {
    }

    public User(String user_id, String lname, String fname, String country, String url, String bio, Date dob, List<String> pics) {
        this.user_id = user_id;
        this.lname = lname;
        this.fname = fname;
        this.country = country;
        this.url = url;
        this.bio = bio;
        this.dob = dob;
        this.pics = pics;
    }

    public User(String user_id, String lname, String fname, String country, String url, String password, String bio, byte[] salt, int iteration, java.sql.Date dob, List<String> pics) {
        this.user_id = user_id;
        this.lname = lname;
        this.fname = fname;
        this.country = country;
        this.url = url;
        this.password = password;
        this.bio = bio;
        this.salt = salt;
        this.iteration = iteration;
        this.dob = dob;
        this.pics = pics;
    }

    public User(String user_id, String lname, String fname, String country, String url, String password, String bio, java.sql.Date dob) {
        this.user_id = user_id;
        this.lname = lname;
        this.fname = fname;
        this.country = country;
        this.url = url;
        this.password = password;
        this.bio = bio;
        this.salt = salt;
        this.iteration = iteration;
        this.dob = dob;
    }
    public List<String> getPics() {
        return pics;
    }

    public void setPics(List<String> pics) {
        this.pics = pics;
    }

    public java.sql.Date getDob() {
        return dob;
    }

    public void setDob(java.sql.Date dob) {
        this.dob = dob;
    }

    public String getuser_id() {
        return user_id;
    }

    public void setuser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public int getIteration() {
        return iteration;
    }

    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}

package ictgradschool.project.servlets.Comments;

import java.io.Serializable;
import java.sql.Timestamp;

public class Comment implements Serializable {
    private int commentID, comment_to, atricle_id;
    private String comment, comment_BY, reply_to, userpic_url;
    private Timestamp comment_time;

    public Comment() {
    }

    public Comment(int commentID, int comment_to, int atricle_id, String comment, String comment_BY, java.sql.Timestamp comment_time) {
        this.commentID = commentID;
        this.comment_to = comment_to;
        this.atricle_id = atricle_id;
        this.comment = comment;
        this.comment_BY = comment_BY;
        this.comment_time = comment_time;
    }

    public String getUserpic_url() {
        return userpic_url;
    }

    public void setUserpic_url(String userpic_url) {
        this.userpic_url = userpic_url;
    }

    public String getReply_to() {
        return reply_to;
    }

    public void setReply_to(String reply_to) {
        this.reply_to = reply_to;
    }

    public int getAtricle_id() {
        return atricle_id;
    }

    public void setAtricle_id(int atricle_id) {
        this.atricle_id = atricle_id;
    }

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public int getComment_to() {
        return comment_to;
    }

    public void setComment_to(int comment_to) {
        this.comment_to = comment_to;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment_BY() {
        return comment_BY;
    }

    public void setComment_BY(String comment_BY) {
        this.comment_BY = comment_BY;
    }

    public java.sql.Timestamp getComment_time() {
        return comment_time;
    }

    public void setComment_time(java.sql.Timestamp comment_time) {
        this.comment_time = comment_time;
    }


}

package ictgradschool.project.servlets.Articles;

import ictgradschool.project.servlets.Accounts.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


@WebServlet(name = "EditArticleMaker")
public class EditArticleServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int articleID = Integer.parseInt(request.getAttribute("articleID").toString());
        HttpSession session = request.getSession();
        ArticleDAO dao = new ArticleDAO(getServletContext());
        Article article = dao.getArticleByID(articleID);
        request.setAttribute("article", article);
        request.getRequestDispatcher("EditArticle.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String content = request.getParameter("content");
        int articleID = Integer.parseInt(request.getParameter("articleID"));
        String publish_time = request.getParameter("publish_time");
        String banner = request.getParameter("banner");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date javaPT = null;
        try {
            javaPT = format.parse(publish_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.sql.Date sqlPT = new java.sql.Date(javaPT.getTime());

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        ArticleDAO dao = new ArticleDAO(getServletContext());
        dao.replaceArticle(articleID, title, content, sqlPT,banner);

        ArrayList<Article> articles = dao.getArticles(user);
        request.setAttribute("articles", articles);
        dao.getArticles(user);

        request.getRequestDispatcher("ArticleView.jsp").forward(request, response);
    }
}

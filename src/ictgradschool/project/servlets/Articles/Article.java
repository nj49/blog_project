package ictgradschool.project.servlets.Articles;

import java.io.Serializable;
import java.sql.Date;

public class Article implements Serializable, Comparable<Article> {
    private String title, content, user_id, userPicURL, banner;
    private int articleID, votes;
    private Date publish_time;

    public Article() {
    }

    public String getBanner() { return banner; }

    public void setBanner(String banner) { this.banner = banner; }

    public String getUserPicURL() {
        return userPicURL;
    }

    public void setUserPicURL(String userPicURL) {
        this.userPicURL = userPicURL;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public Date getPublish_time() {
        return publish_time;
    }

    public void setPublish_time(Date publish_time) {
        this.publish_time = publish_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getArticleID() {
        return articleID;
    }

    public void setArticleID(int articleID) {
        this.articleID = articleID;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    @Override
    public int compareTo(Article o) {
        if (getPublish_time() == null || o.getPublish_time() == null) {
            return 0;
        }
        return getPublish_time().compareTo(o.getPublish_time());
    }
}

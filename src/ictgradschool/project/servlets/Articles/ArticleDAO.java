package ictgradschool.project.servlets.Articles;

import ictgradschool.project.servlets.Accounts.User;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class ArticleDAO {
    private Properties dbProps;
    private ServletContext context;

    public ArticleDAO(ServletContext context) {
        this.context = context;
        dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/dbProps.properties"))) {//
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }
    }

    public int addArticle(String title, User user, String content, java.sql.Date publish_time) {
        int articleID = 0;
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("INSERT INTO group_project_article(title, user_id, content,publish_time,banner) VALUES (?,?,?,?,'Photos/Banner_1.jpg')", Statement.RETURN_GENERATED_KEYS)) {
                Stmt.setString(1, title);
                Stmt.setString(2, user.getuser_id());
                Stmt.setString(3, content);
                Stmt.setDate(4, (java.sql.Date) publish_time);

                Stmt.executeUpdate();
                try (ResultSet rs = Stmt.getGeneratedKeys();) {
                    if (rs.next()) {
                        articleID = rs.getInt(1);
                    }
                }
                return articleID;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return articleID;
    }


    public ArrayList<Article> getArticles(User user) {
        Article article = null;
        ArrayList<Article> articlelist = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_article WHERE user_id= ?")) {
                Stmt.setString(1, user.getuser_id());

                try (ResultSet rs = Stmt.executeQuery()) {
                    int titleCol = rs.findColumn("title");
                    int votesCol = rs.findColumn("votes");
                    int contentCol = rs.findColumn("content");
                    int publish_dateCol = rs.findColumn("publish_time");
                    while (rs.next()) {
                        article = new Article();
                        article.setArticleID(rs.getInt(1));
                        article.setUser_id(user.getuser_id());
                        article.setTitle(rs.getString(titleCol));
                        article.setContent(rs.getString(contentCol));
                        article.setPublish_time(rs.getDate(publish_dateCol));
                        article.setVotes(rs.getInt(votesCol));
                        articlelist.add(article);
                    }
                    Collections.sort(articlelist);
                    Collections.reverse(articlelist);
                    return articlelist;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return articlelist;
    }


    public ArrayList<Article> getHPArticles() {
        Article article = null;
        ArrayList<Article> articlelist = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_article")) {
                try (ResultSet rs = Stmt.executeQuery()) {
                    int titleCol = rs.findColumn("title");
                    int userCol = rs.findColumn("user_id");
                    int contentCol = rs.findColumn("content");
                    int publish_dateCol = rs.findColumn("publish_time");
                    int votesCol = rs.findColumn("votes");
                    int bannerCol = rs.findColumn("banner");
                    while (rs.next()) {
                        article = new Article();
                        article.setArticleID(rs.getInt(1));
                        article.setUser_id(rs.getString(userCol));
                        article.setTitle(rs.getString(titleCol));
                        article.setContent(rs.getString(contentCol));
                        article.setPublish_time(rs.getDate(publish_dateCol));
                        article.setVotes(rs.getInt(votesCol));
                        article.setBanner(rs.getString(bannerCol));
                        articlelist.add(article);
                    }
                    Collections.sort(articlelist);
                    Collections.reverse(articlelist);
                    return articlelist;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return articlelist;
    }

    public Article getArticleByID(int id) {
        Article article = null;
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement(" SELECT DISTINCT articles.banner,articles.user_id,articles.articel_id, articles.title, articles.content, articles.publish_time, articles.votes,articleUser.profile_pic_url FROM group_project_article AS articles, group_project_user_db AS articleUser WHERE articles.articel_id = ? AND articleUser.user_id = articles.user_id;")) {
                Stmt.setInt(1, id);
                try (ResultSet rs = Stmt.executeQuery()) {
                    int titleCol = rs.findColumn("title");
                    int userCol = rs.findColumn("user_id");
                    int contentCol = rs.findColumn("content");
                    int votesCol = rs.findColumn("votes");
                    int publishtimeCol = rs.findColumn("publish_time");
                    int articleidCol = rs.findColumn("articel_id");
                    int userpicCol = rs.findColumn("profile_pic_url");
                    int bannerCol = rs.findColumn("banner");
                    while (rs.next()) {
                        article = new Article();
                        article.setArticleID(rs.getInt(articleidCol));
                        article.setUser_id(rs.getString(userCol));
                        article.setTitle(rs.getString(titleCol));
                        article.setContent(rs.getString(contentCol));
                        article.setPublish_time(rs.getDate(publishtimeCol));
                        article.setVotes(rs.getInt(votesCol));
                        article.setUserPicURL(rs.getString(userpicCol));
                        article.setBanner(rs.getString(bannerCol));
                    }
                    return article;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return article;
    }


    public void replaceArticle(int id, String title, String content, java.sql.Date publish_time,String banner) {
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("UPDATE group_project_article SET content = ?, title = ?,publish_time=?,banner=? WHERE articel_id= ? ")) {
                Stmt.setString(1, content);
                Stmt.setString(2, title);
                Stmt.setDate(3, (java.sql.Date) publish_time);
                Stmt.setString(4,banner);
                Stmt.setInt(5, id);
                Stmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //method for checking if user is allowed to delete a given article. called by the delArticle() method below
    public boolean canDeleteArticle(int articleID, String user_id) {
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT DISTINCT user_id FROM group_project_article AS articles WHERE articles.articel_id = ?;")) {
                Stmt.setInt(1, articleID);
                try (ResultSet rs = Stmt.executeQuery()) {
                    int articleuser_idCol = rs.findColumn("user_id");
                    while (rs.next()) {
                        if ((rs.getString(articleuser_idCol).equals(user_id))) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public boolean delArticle(int articleID, String user_id) {
        if (canDeleteArticle(articleID, user_id)) {
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                try (PreparedStatement Stmt = conn.prepareStatement("DELETE FROM group_project_article WHERE articel_id = ?;")) {
                    Stmt.setInt(1, articleID);
                    Stmt.execute();
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            return false;
        }
        return false;
    }

    public ArrayList<Article> getADSearchedArticles(String ADsearchString) {
        Article article = null;
        ArrayList<Article> articlelist = new ArrayList<>();
        //set first list item as "no results found" titled Article
        Article noResults = new Article();
        noResults.setTitle("No Results found");
        articlelist.add(noResults);
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_article WHERE title like ? OR user_id like ?")) {
                Stmt.setString(1, "%" + ADsearchString + "%");
                Stmt.setString(2, "%" + ADsearchString + "%");
                try (ResultSet rs = Stmt.executeQuery()) {
                    int titleCol = rs.findColumn("title");
                    int userCol = rs.findColumn("user_id");
                    int contentCol = rs.findColumn("content");
                    int publish_dateCol = rs.findColumn("publish_time");
                    int votesCol = rs.findColumn("votes");
                    while (rs.next()) {
                        article = new Article();
                        article.setArticleID(rs.getInt(1));
                        article.setUser_id(rs.getString(userCol));
                        article.setTitle(rs.getString(titleCol));
                        article.setContent(rs.getString(contentCol));
                        article.setPublish_time(rs.getDate(publish_dateCol));
                        article.setVotes(rs.getInt(votesCol));
                        articlelist.add(article);
                    }
                    if (articlelist.size() > 1) {
                        articlelist.remove(0);
                    }
                    return articlelist;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //if there are articles found remove the no results item
        if (articlelist.size() > 1) {
            articlelist.remove(0);
        }
        return articlelist;
    }


    public ArrayList<Article> getDateSearchedArticles(String ADsearchString, String date1, String date2) {
        Article article = null;
        ArrayList<Article> articlelist = new ArrayList<>();
        //set first list item as "no results found" titled Article
        Article noResults = new Article();
        noResults.setTitle("No Results found");
        articlelist.add(noResults);
        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_article WHERE (title like ? OR user_id like ?) AND (publish_time>=? AND publish_time<=?)")) {
                Stmt.setString(1, "%" + ADsearchString + "%");
                Stmt.setString(2, "%" + ADsearchString + "%");
                Stmt.setDate(3, Date.valueOf(date1));
                Stmt.setDate(4, Date.valueOf(date2));
                try (ResultSet rs = Stmt.executeQuery()) {
                    int titleCol = rs.findColumn("title");
                    int userCol = rs.findColumn("user_id");
                    int contentCol = rs.findColumn("content");
                    int publish_dateCol = rs.findColumn("publish_time");
                    int votesCol = rs.findColumn("votes");
                    while (rs.next()) {
                        article = new Article();
                        article.setArticleID(rs.getInt(1));
                        article.setUser_id(rs.getString(userCol));
                        article.setTitle(rs.getString(titleCol));
                        article.setContent(rs.getString(contentCol));
                        article.setPublish_time(rs.getDate(publish_dateCol));
                        article.setVotes(rs.getInt(votesCol));
                        articlelist.add(article);
                    }
                    if (articlelist.size() > 1) {
                        articlelist.remove(0);
                    }
                    return articlelist;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //if there are articles found remove the no results item
        if (articlelist.size() > 1) {
            articlelist.remove(0);
        }
        return articlelist;
    }

    public void voteArticle(String voteDirection, int articleID) {
        int vote = 0;
        if (voteDirection.equals("up")) {
            vote = 1;
        } else if (voteDirection.equals("down")) {
            vote = -1;
        }

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("UPDATE group_project_article SET votes = votes + ? WHERE articel_id=?")) {
                Stmt.setInt(1, vote);
                Stmt.setInt(2, articleID);
                Stmt.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Article> getUpcomingArticles(User user) {
        Article article = null;
        ArrayList<Article> articlelist = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_article WHERE user_id= ? AND publish_time > CURRENT_DATE ")) {
                Stmt.setString(1, user.getuser_id());
                try (ResultSet rs = Stmt.executeQuery()) {
                    int titleCol = rs.findColumn("title");
                    int votesCol = rs.findColumn("votes");
                    int contentCol = rs.findColumn("content");
                    int publish_dateCol = rs.findColumn("publish_time");
                    while (rs.next()) {
                        article = new Article();
                        article.setArticleID(rs.getInt(1));
                        article.setUser_id(user.getuser_id());
                        article.setTitle(rs.getString(titleCol));
                        article.setContent(rs.getString(contentCol));
                        article.setPublish_time(rs.getDate(publish_dateCol));
                        article.setVotes(rs.getInt(votesCol));
                        articlelist.add(article);
                    }
                    Collections.sort(articlelist);
                    Collections.reverse(articlelist);
                    return articlelist;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return articlelist;
    }

    public ArrayList<Article> getArticleByUserID(String articleUserID) {
        Article article = null;
        ArrayList<Article> articlelist = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_article WHERE (user_id= ? AND publish_time<=CURRENT_DATE)")) {
                Stmt.setString(1, articleUserID);
                try (ResultSet rs = Stmt.executeQuery()) {
                    int titleCol = rs.findColumn("title");
                    int votesCol = rs.findColumn("votes");
                    int contentCol = rs.findColumn("content");
                    int publish_dateCol = rs.findColumn("publish_time");
                    while (rs.next()) {
                        article = new Article();
                        article.setArticleID(rs.getInt(1));
                        article.setUser_id(articleUserID);
                        article.setTitle(rs.getString(titleCol));
                        article.setContent(rs.getString(contentCol));
                        article.setPublish_time(rs.getDate(publish_dateCol));
                        article.setVotes(rs.getInt(votesCol));
                        articlelist.add(article);
                    }
                    Collections.sort(articlelist);
                    Collections.reverse(articlelist);
                    return articlelist;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return articlelist;
    }

    public List<Article> getLastTenUserArticles(User user) {
        Article article = null;
        ArrayList<Article> articlelist = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("(SELECT articel_id,title,votes FROM group_project_article WHERE (user_id= ? AND publish_time<=CURRENT_DATE) ORDER BY articel_id DESC LIMIT 10) ORDER BY articel_id")) {
                Stmt.setString(1, user.getuser_id());
                try (ResultSet rs = Stmt.executeQuery()) {
                    int titleCol = rs.findColumn("title");
                    int votesCol = rs.findColumn("votes");
                    int articleidCol = rs.findColumn("articel_id");
                    while (rs.next()) {
                        article = new Article();
                        article.setArticleID(rs.getInt(articleidCol));
                        article.setUser_id(user.getuser_id());
                        article.setTitle(rs.getString(titleCol));
                        article.setVotes(rs.getInt(votesCol));
                        articlelist.add(article);
                    }
                    return articlelist;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return articlelist;
    }
}


